package br.com.vote.repository;

import br.com.vote.model.Associated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssociatedRepository extends JpaRepository<Associated, Long> {
    Associated findByCpf(String cpf);
}