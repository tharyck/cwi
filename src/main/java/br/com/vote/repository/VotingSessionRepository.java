package br.com.vote.repository;

import br.com.vote.model.VotingSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VotingSessionRepository extends JpaRepository<VotingSession, Long> {
    VotingSession findByScheduleId(Long scheduleId);
}