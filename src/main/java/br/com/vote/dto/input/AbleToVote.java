package br.com.vote.dto.input;

public class AbleToVote {

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
