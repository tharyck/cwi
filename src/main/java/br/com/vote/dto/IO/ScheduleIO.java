package br.com.vote.dto.IO;

import br.com.vote.dto.input.ScheduleInput;
import br.com.vote.model.Associated;
import br.com.vote.model.Schedule;
import br.com.vote.model.ScheduleStatus;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Mapper to Schedule.
 */
@Component("scheduleIO")
public class ScheduleIO {
	private ModelMapper modelMapper;

	final Converter<ScheduleInput, Schedule> scheduleConverter = new Converter<ScheduleInput, Schedule>() {
		@Override
		public Schedule convert(MappingContext<ScheduleInput, Schedule> context) {
			ScheduleInput scheduleInput = context.getSource();
			// @formatter:off
			return new Schedule(
					scheduleInput.getName(),
					scheduleInput.getDescription(),
					ScheduleStatus.CREATED);
			// @formatter:on
		}
	};

	public ScheduleIO() {
		modelMapper = new ModelMapper();
		modelMapper.addConverter(scheduleConverter);
	}

	public Schedule mapTo(ScheduleInput scheduleInput) {
		return this.modelMapper.map(scheduleInput, Schedule.class);
	}
}