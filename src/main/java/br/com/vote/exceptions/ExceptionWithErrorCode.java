package br.com.vote.exceptions;

public interface ExceptionWithErrorCode {
	public String getErrorCode();
}
