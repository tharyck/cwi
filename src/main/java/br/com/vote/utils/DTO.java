package br.com.vote.utils;

/**
 * Defines interface to transfer objects.
 *
 * @param <T>
 */
public interface DTO<T> {

	public T toModel();

}
